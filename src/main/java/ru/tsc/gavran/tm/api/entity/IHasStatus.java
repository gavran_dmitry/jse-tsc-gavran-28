package ru.tsc.gavran.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import ru.tsc.gavran.tm.enumerated.Status;

public interface IHasStatus {

    @NotNull
    Status getStatus();

    void setStatus(@NotNull Status status);

}
