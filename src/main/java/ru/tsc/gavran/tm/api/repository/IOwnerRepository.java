package ru.tsc.gavran.tm.api.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.gavran.tm.api.IRepository;
import ru.tsc.gavran.tm.model.AbstractOwnerEntity;

import java.util.Comparator;
import java.util.List;

public interface IOwnerRepository<E extends AbstractOwnerEntity> extends IRepository<E> {

    @NotNull
    E add(@NotNull String userId, @NotNull final E entity);

    void remove(@NotNull String userId, @NotNull final E entity);

    @NotNull
    List<E> findAll(@NotNull String userId);

    void clear(@NotNull String userId);

    @Nullable
    E findById(@NotNull String userId, @NotNull final String id);

    @Nullable
    E removeById(@NotNull String userId, @NotNull final String id);

    @Nullable
    E findByIndex(@NotNull String userId, @NotNull final Integer index);

    @NotNull
    List<E> findAll(@NotNull String userId, @NotNull final Comparator<E> comparator);

    boolean existsById(@NotNull String userId, @NotNull final String id);

    @Nullable
    E removeByIndex(@NotNull String userId, @NotNull final Integer index);

}
