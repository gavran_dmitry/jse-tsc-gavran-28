package ru.tsc.gavran.tm.repository;

import lombok.Getter;
import lombok.Setter;
import ru.tsc.gavran.tm.api.repository.IAuthRepository;

@Setter
@Getter
public class AuthRepository implements IAuthRepository {

    private String currentUserId;

}
